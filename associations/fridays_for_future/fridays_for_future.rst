.. index::
   ! Fridays for future Grenoble

.. _fff_grenoble:

=========================================================
Fridays for future Grenoble
=========================================================

.. seealso::

   - https://twitter.com/fff_grenoble
   - https://www.facebook.com/FridaysForFutureGrenoble/
