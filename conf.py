#
# Configuration file for the Sphinx documentation builder.
# http://www.sphinx-doc.org/en/stable/config

import sys, os
import pendulum

project = "Urgence climatique!!"
html_title = project
author = "Humain(e)s"
now = pendulum.now("Europe/Paris")
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H{now.minute:02}"
release = version
copyright = f"2019-{now.year}, {author} Creative Commons CC BY-NC-SA 3.0"
source_suffix = ".rst"
master_doc = "index"
language = None
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
html_theme = "default"
pygments_style = "sphinx"
extensions = ["sphinx.ext.intersphinx"]
intersphinx_mapping = {
#    "https://gfagrenoble.frama.io/fagrenoble": None,
#    "https://gassr38.frama.io/livret_accueil": None,
}
