# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = Djangotutorials
SOURCEDIR     = .
BUILDDIR      = _build

THIS_MAKEFILE := $(lastword $(MAKEFILE_LIST))

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	@echo " "
	@echo "Targets:"
	@echo " "
	@echo "- make check_all"
	@echo "- make req"
	@echo "- make updatetools"
	@echo "- make update"
	@echo " "

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)


check_all:
	pre-commit run --all-files

req:
	pipenv --venv
	pipenv graph
	pipenv check
	pipenv lock -r > requirements.txt;
	cat requirements.txt
	git diff requirements.txt

update:
	pipenv update
	@$(MAKE) -f $(THIS_MAKEFILE) req

updatetools:
	pre-commit autoupdate
	gitmoji --update
	git status
