.. index::
   ! Robin Jullian

.. _robin_jullian:

=========================================================
Robin Jullian (Grenoble)
=========================================================

.. seealso::

   - https://twitter.com/RobinJullian/


.. toctree::
   :maxdepth: 3

   actions/actions
