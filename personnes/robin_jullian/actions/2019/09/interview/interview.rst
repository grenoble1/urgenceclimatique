.. index::
   pair: Interview ; Robin Jullian (2019-09)

.. _interview_robin_jullian_2019_9:

===============================================================
Interview de Robin Jullian par ici-grenoble en septembre 2019
===============================================================

.. seealso::

   - http://ici-grenoble.org/agenda/evenement.php?id=9347
   - https://france3-regions.francetvinfo.fr/auvergne-rhone-alpes/isere/grenoble/greta-thunberg-jeune-iserois-robin-jullian-se-bat-climat-1719795.html
   - https://www.facebook.com/FridaysForFutureGrenoble/
   - http://www.ici-grenoble.org/infospratiques/fiche.php?id=9351
   - https://www.youtube.com/watch?v=DE0lfkOrEZ4

ici Grenoble : Pourriez-vous vous présenter à nos lecteurs et nos lectrices ?

Robin Jullian : J’ai 17 ans et je suis l’un des initiateurs et activiste des
mouvements Fridays For Future et Youth for Climate en France.

Quand avez-vous commencé à faire la grève scolaire pour le Climat ?

J’ai commencé le 25 janvier 2019, mais je ne compte pas cette grève car je n’avais
pas de panneaux, nis de photos : j’avais juste discuté avec quelques camarades
sur les problématiques environnementales.
Ma vraie première grève, c'était le premier février 2019.

Qu'est-ce qui a "déclenché" cette décision ? Un livre ? Un film ?
L'exemple de Greta Thunberg ?

Vers l’âge de sept ans, j’ai appris qu’il y avait un réchauffement global causé
par l’humain, et que ce réchauffement était la cause de pas mal de problèmes.
Mais à cet âge-là, je n’étais pas encore conscient de l’urgence qu’il y avait.
Cela ne m’empêchait pas de me disputer avec mes camarades de classe qui
jetaient des emballages par terre...

À l’âge de 12 ans, en cinquième, ma professeure d’Histoire-Géographie nous a
montré un documentaire sur les effets du réchauffement climatique et l’effondrement
de la biodiversité. Après cette vidéo, j’ai fait une sorte de dépression.
Je n’avais plus foi en l’humanité.

Après quelques mois assez difficiles, je me suis finalement dit que je pouvais
faire quelque chose. Alors j’ai décidé d’en apprendre plus sur les causes de
ce dérèglement, et comment on pouvait les régler avec des alternatives.

Quelques années plus tard, en janvier 2019, j’ai vu le discours de
Greta Thunberg à la COP24. À ce moment, tous mes plans pour "sauver le monde"
en 70 ans se sont totalement effondrés.

Les jours suivants, j’ai beaucoup réfléchi.

Je n’ai dormi que très peu. Et finalement, je me suis dit que ce mouvement des
grèves pour le Climat avait le pouvoir d’impulser quelque chose de grand,
un changement global et rapide. Alors je me suis assis dans la cour de mon
lycée, et c’est là que tout a commencé.

Pourquoi vous et pas un-e autre ? Avez-vous une idée des raisons pour
lesquelles vous, Robin Jullian, vous avez senti l'urgente nécessité de vous
engager pour le Climat, et pas d'autres lycéen-ne-s ?

Je pense que mon engagement est lié à mon éducation. J’ai vécu et je vis encore
en montagne. Je suis donc aux premières loges des changements climatiques.

Il est aussi possible que je sois plus sensible que d'autres personnes au
désastre en cours. Là où d'autres voient cette crise comme un petit problème,
**moi je la perçois comme une urgence, et je n’arrive pas à comprendre pourquoi
les autres ne font rien alors que les faits sont là**.

Du coup, j’agis comme je peux.
Je ne me suis jamais considéré comme "important". Pour moi, je suis là pour
aider les autres, alors je ferai tout ce qui est possible pour faire une
différence, pour que l’humanité mais aussi toute la biodiversité dont nous
faisons partie et dont nous dépendons ne disparaisse pas.

Vos parents sont-ils déjà engagés dans ces luttes ? Ou des ami-e-s proches ?

Mes parents ne sont pas engagés, mais ils sont déjà sensibilisés.
Mon père est apiculteur.

Et oui, j’ai des ami-e-s engagé-e-s, et ce sont toutes de formidables personnes.
Elles font partie de Friday for Future Grenoble. Elles restent dans l’ombre
malgré leur travail que j’admirerai toujours : Stéphane, Marine, Arnaud, Damien,
John, Thomas, Mathieu, Anouk et tous ceux qui ont permis de devenir ce que
nous sommes aujourd’hui !

Pourquoi faire grève devant la mairie de Grenoble ?
======================================================

Au tout début, j’ai fait grève dans la cour de mon lycée à Vizille.
J’ai essayé de bouger les autres, mais c'était très difficile. Alors j’ai fait
grève devant l’hôtel de ville de Grenoble, et à ce moment les choses sont
allées beaucoup plus vite.

Désormais, tous les vendredis après-midi, je fais grève devant la mairie de
Grenoble, seul ou avec d’autres activistes.
On débat souvent sur les problèmes de société et de comment on pourrait
y pallier. C’est plutôt enrichissant de rencontrer des personnes avec des
avis différents.

Comment vos professeurs et votre proviseur ont-ils réagi ? Ils ont pris des
sanctions contre vous, ou est-ce qu'ils vous encouragent ?

Pour l’instant, je n’ai eu que des réponses positives et des encouragements.
J’en déduis qu’ils comprennent le message. D'ailleurs, j’espère les voir
ce vendredi 20 septembre 2019 !

Comment réagissent les autres lycéen-ne-s ? Est-ce que vous sentez une évolution
au sein du lycée, des prises de conscience de plus en plus fortes, ou pas tellement ?

Dans mon lycée, c’est un peu compliqué.
Ma classe est un peu plus sensibilisée depuis que je fais grève. J’essaie de
les bouger pour qu’ils changent un peu leurs habitudes, mais ça reste compliqué, oui.

Après, c’est sûr que ce n’est pas simple de quitter un peu de confort, et donc
je n’en veux pas à celles et ceux qui ont du mal. Le confort, c’est un peu
comme une drogue, on n'en a pas forcément besoin. Mais dès qu’on en a, c’est
compliqué d’arrêter. Il faut aussi ne surtout pas oublier les conditions
sociales qui peuvent rendre plus complexes une transition.

Comment faites-vous pour rattraper les cours ? Vous avez du soutien d'autres lycéen-ne-s ?
============================================================================================

J’ai des amis qui prennent les photos des cours et qui me les envoient pour que
je les rattrape.

Faites-vous partie d'Extinction Rebellion ?
==============================================

Oui, je participe beaucoup aux actions d’Extinction Rebellion, qui est un
mouvement avec qui Fridays For Future s’entend très bien.

Êtes-vous en lien avec d'autres lycéen-ne-s ailleurs dans le monde, engagé-e-s comme vous ?
=============================================================================================

Oui, je suis dans les groupes de coordination internationale de Fridays For Future.

Qu'est-ce qui vous marque dans ces échanges mondiaux ?
=======================================================

Ce qui me touche le plus, c’est que malgré nos différences dans notre éducation
et dans notre culture, on perçoit toutes et tous les mêmes problèmes sociétaux.
On a peur pour celles et ceux qu’on aime, mais aussi pour l’avenir de l’humanité.

Depuis le début de votre engagement pour le Climat, qu'est-ce qui a changé dans votre manière de voir le monde ?
===================================================================================================================

Ma manière de voir le monde n’a pas vraiment changé, mais une petite lueur
d’espoir est apparue quand j’ai vu que nous étions plus de 3 millions à marcher
dans le monde le 15 mars 2019.

Quand j’étais à Aachen (Aix-La-Chapelle) pour la première manifestation Européenne
centralisée, j’ai aussi vu que les frontières et les langues, ces barrières qui
nous divisent, sont finalement oubliées pour un seul et même combat qui nous
touche déjà toutes et tous.

Est-ce que vous avez changé des choses dans votre mode de vie ?
==================================================================

Oui, j’ai pris les habitudes qu’on voit un peu partout : je suis végétarien, je
n’achète rien de nouveau sauf quand cela est vraiment nécessaire, je fais le tri, etc.

Qu'est-ce que vous diriez à des lycéen-ne-s pour les encourager à participer aux prochaines Marches pour le Climat ?
=====================================================================================================================

Je leur dirais de se renseigner, d’apprendre, de lire et de prendre du recul.

Je leur dirais de se rendre compte de l’urgence, qu’ils ne peuvent pas abandonner
comme ça un monde au bord de l’agonie, et que nous pouvons gagner seulement
si nous restons uni-e-s.

Ce que je viens de dire compte aussi pour les adultes !

Du fait de votre médiatisation, des partis politiques vont probablement vous
approcher pour vous proposer de rejoindre leurs listes électorales.
Est-ce que vous souhaiteriez vous engager dans un parti, ou est-ce que vous
êtes plutôt attirés par des engagements associatifs ou libertaires ?

Pour moi, aucune idéologie existante ne fonctionne, aucun parti politique ne
sauvera le monde. Les partis politiques ont des idées beaucoup trop fermées,
ce qui divise les gens.
En fait, je pense que donner une direction idéologique est contre-productif.

Et pour la médiatisation, je vais faire beaucoup plus attention, car dans la
mobilisation française être surmédiatisé est mal vu.
Je vais aussi me concentrer sur les interviews qui parlent de la crise
et non de moi.

Une dernière question personnelle, alors : qu'est-ce que vous aimeriez faire d'ici quelques années ?
====================================================================================================

Cette une question assez complexe ! Tout dépend de ce qui se passe dans les dix
prochaines années...

Mais personnellement, j’ai toujours rêvé de créer une ONG ou une Fondation qui
produirait avec l’aide de scientifiques des solutions beaucoup plus durables
pour les besoins de bases de l’humanité, tout en utilisant les low-techs.

D'ailleurs, je travaille déjà sur un système de stockage partagé qui permettra
de se passer de serveur pour internet, et donc d’utiliser beaucoup moins d’énergie.

Sinon, dans un plus court terme, je compte avoir mon BAC professionnel cette
année, avec mention si possible, et continuer mes études en électronique et
informatique dans la région, ou à l’étranger si je peux me le permettre.

Si vous aviez un film, un site internet et un livre marquants à conseiller à
des personnes peu sensibilisées aux questions climatiques, ce serait lesquels ?

Mes références sont les films de Cyril Dion, Demain et Après Demain, qui
donnent un peu d'espoir et qui montrent celles et ceux qui se bougent et
qu’on ne voit pas forcément.

Pour les plus courageux et courageuses, je recommande le rapport SR15 du GIEC.
Je trouve qu'il n’y a rien de mieux pour en apprendre sur le dérèglement climatique.

Il y a aussi le livre de Greta Thunberg, Rejoignez-nous. Je ne l’ai pas encore lu,
mais la connaissant et connaissant un peu le contenu de ce livre, je pense
que ça peut en toucher beaucoup plus d’un-e.

Et pour finir, je conseille un film qui m’a beaucoup touché : Interstellar.
Certes, ce film part dans un délire de quitter notre planète, mais au début
du film on voit un futur proche qui pour moi est très réaliste.
C'est d'ailleurs ce que vivent déjà certaines populations.

Ce film m’a aussi permis de me rendre compte de la grandeur de l’univers, et
à quel point nous sommes petits, à quel point nous ne sommes rien.

Je pense que lever les yeux au ciel et regarder les étoiles pourrait aider
certaines personnes, nos politiciens par exemple, qui vivent pour leur ego,
de se rendre compte que nous ne sommes qu’une fraction de seconde pour
l’univers, et que nous sommes perdu-e-s au milieu de rien.

Pour l’instant, la seule trace de vie qu’on connaisse se trouve ici, sur Terre,
alors pourquoi vivre au détriment de cet écosystème fragile alors qu’on peut
vivre avec et même l’aider à se développer dans cet univers froid ?

Un dernier mot ?
=================

Cela fait plus d’un an maintenant que Fridays for Future existe.
La jeunesse du monde entier s'est levée par millions, faisant tomber les
frontières pour sauver un monde à l’agonie.

Mais seul-e-s, nous n’y arriverons pas.

Alors étudiant-e-s, travailleurs et travailleuses, habitant-e-s de cette
planète, joignons-nous, convergeons pour une justice climatique et sociale,
pour sauver ce qui peut être sauvé, pour s’entraider et pour bâtir le monde
dans lequel nous voulons vivre, un monde où les générations futures de toutes
les espèces n’auront plus à se soucier de leurs avenir : rejoignez-nous !

* * *

Cette interview vous enthousiasme, vous intrigue ou vous questionne ?
Vous avez envie d'en savoir plus ou de débattre de certaines idées exprimées ici ?

N'hésitez pas à rejoindre Robin Jullian et ses camarades pour discuter avec
lui lors d'une Grève scolaire pour le Climat, un vendredi après-midi sur le
parvis de la mairie de Grenoble...

